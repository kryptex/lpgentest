from django.apps import apps
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import Http404, HttpResponse
from django.views.generic import TemplateView, ListView, DetailView, FormView, RedirectView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from University import models, forms
import json
from collections import OrderedDict


class Index(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['title'] = 'Главная'
        return context


class Login(FormView):
    template_name = 'registration/login.html'
    form_class = forms.AuthenticationForm
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        context = super(Login, self).get_context_data(**kwargs)
        context['title'] = 'Авторизация'
        context['action'] = 'Войти'
        context['login'] = True
        return context

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            login(request, user)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class Logout(RedirectView):
    def get(self, request, *args, **kwargs):
        logout(request)
        self.url = reverse_lazy('index')
        return super(Logout, self).get(request, *args, **kwargs)


class Register(FormView):
    template_name = 'registration/login.html'
    form_class = forms.UserCreationForm
    success_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super(Register, self).get_context_data(**kwargs)
        context['title'] = 'Регистрация'
        context['action'] = 'Зарегистрироваться'
        return context

    def form_valid(self, form):
        form.save()
        return super(Register, self).form_valid(form)


class BaseListView(ListView):
    """
    Базовый класс, наследующий ListView, для того, чтобы динамически определять title у наследников и сортировать
    queryset по полю name.
    """
    template_name = 'listview.html'

    def get_context_data(self, **kwargs):
        context = super(BaseListView, self).get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name_plural
        return context

    def get_queryset(self):
        return self.model.objects.order_by('name')


class ProfessorList(BaseListView):
    model = models.Professor


class FacultyList(BaseListView):
    model = models.Faculty


class StudentList(BaseListView):
    model = models.Student


class SubjectList(BaseListView):
    model = models.Subject


class BaseDetailView(DetailView):
    """
    Базовый класс, наследующий DetailView, для того, чтобы динамически определять title у наследников и сериализовать
    поля объекта для отображения в шаблоне.
    """
    template_name = 'detailview.html'

    def get_context_data(self, **kwargs):
        context = super(BaseDetailView, self).get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name
        return context

    def get_object(self, queryset=None):
        obj = super(BaseDetailView, self).get_object(queryset)
        serialized = OrderedDict()
        for field in self.model._meta.get_fields():
            if not field.is_relation and field.name != 'id':
                serialized.update({field.verbose_name: getattr(obj, field.name, None)})
        return serialized


class ProfessorDetail(BaseDetailView):
    model = models.Professor

    def get_context_data(self, **kwargs):
        context = super(ProfessorDetail, self).get_context_data(**kwargs)
        context['details_header'] = 'Список студентов преподавателя'
        context['details'] = models.Student.objects.filter(subjectstudent__subject__professor=self.kwargs['pk']).order_by('name').all()
        return context


class FacultyDetail(BaseDetailView):
    model = models.Faculty

    def get_context_data(self, **kwargs):
        context = super(FacultyDetail, self).get_context_data(**kwargs)
        context['details_header'] = 'Список студентов на факультете'
        context['details'] = models.Student.objects.filter(subjectstudent__subject__professor__faculty=self.kwargs['pk']).order_by('name').all()
        return context


class StudentDetail(BaseDetailView):
    model = models.Student

    def get_context_data(self, **kwargs):
        context = super(StudentDetail, self).get_context_data(**kwargs)
        context['details_header'] = 'Список дисциплин студента'
        context['details'] = models.Subject.objects.filter(subjectstudent__student=self.kwargs['pk']).order_by('name').all()
        return context


class SubjectDetail(BaseDetailView):
    model = models.Subject

    def get_context_data(self, **kwargs):
        context = super(SubjectDetail, self).get_context_data(**kwargs)
        context['details_header'] = 'Список преподавателей предмета'
        context['details'] = models.Professor.objects.filter(subject=self.kwargs['pk']).order_by('name').all()
        return context


class BaseCreateView(LoginRequiredMixin, CreateView):
    """
    Базовый класс, наследующий CreateView, для того, чтобы динамически определять title, action и success_url
    у наследников.
    Также наследуется от LoginRequiredMixin, для чтобы с View могли работать только авторизованные пользователи.
    """
    template_name = 'changeview.html'
    success_url = ''

    def __init__(self):
        super(BaseCreateView, self).__init__()
        self.success_url = reverse_lazy(self.get_form_class()._meta.model._meta.object_name.lower()+'-list')

    def get_context_data(self, **kwargs):
        context = super(BaseCreateView, self).get_context_data(**kwargs)
        context['title'] = self.get_form_class()._meta.model._meta.verbose_name
        context['action'] = 'Добавить'
        return context


class ProfessorCreate(BaseCreateView):
    form_class = forms.ProfessorForm


class FacultyCreate(BaseCreateView):
    form_class = forms.FacultyForm


class SubjectCreate(BaseCreateView):
    form_class = forms.SubjectForm


class StudentCreate(BaseCreateView):
    form_class = forms.StudentForm


class BaseUpdateView(LoginRequiredMixin, UpdateView):
    """
    Базовый класс, наследующий UpdateView, для того, чтобы динамически определять title, action и success_url
    у наследников.
    Также наследуется от LoginRequiredMixin, для чтобы с View могли работать только авторизованные пользователи.
    """
    template_name = 'changeview.html'
    success_url = ''

    def __init__(self):
        super(BaseUpdateView, self).__init__()
        self.success_url = reverse_lazy(self.model._meta.object_name.lower()+'-list')

    def get_context_data(self, **kwargs):
        context = super(BaseUpdateView, self).get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name
        context['action'] = 'Изменить'
        return context


class ProfessorUpdate(BaseUpdateView):
    form_class = forms.ProfessorForm
    model = models.Professor


class FacultyUpdate(BaseUpdateView):
    form_class = forms.FacultyForm
    model = models.Faculty


class SubjectUpdate(BaseUpdateView):
    form_class = forms.SubjectForm
    model = models.Subject


class StudentUpdate(BaseUpdateView):
    form_class = forms.StudentForm
    model = models.Student


class BaseDeleteView(LoginRequiredMixin, DeleteView):
    """
    Базовый класс, наследующий DeleteView, для того, чтобы динамически определять title, action и success_url
    у наследников.
    Также наследуется от LoginRequiredMixin, для чтобы с View могли работать только авторизованные пользователи.
    """
    template_name = 'deleteview.html'
    success_url = ''

    def __init__(self):
        super(BaseDeleteView, self).__init__()
        self.success_url = reverse_lazy(self.model._meta.object_name.lower() + '-list')

    def get_context_data(self, **kwargs):
        context = super(BaseDeleteView, self).get_context_data(**kwargs)
        context['title'] = self.model._meta.verbose_name
        context['action'] = 'Удалить'
        return context


class ProfessorDelete(BaseDeleteView):
    model = models.Professor


class FacultyDelete(BaseDeleteView):
    model = models.Faculty


class SubjectDelete(BaseDeleteView):
    model = models.Subject


class StudentDelete(BaseDeleteView):
    model = models.Student


def get_chart(request):
    """
    Генерация данных для графика на главной странице
    :return: данные в json для обработки в скрипте
    """
    if request.is_ajax():
        points = []
        app = apps.get_app_config('University')
        for model in app.models.values():
            ct = ContentType.objects.get_for_model(model)
            ss = ContentType.objects.get_for_model(models.SubjectStudent)
            if not ct == ss:
                points.append({'label': model._meta.verbose_name, 'y': model.objects.count()})

        data = {'chart':  {
            'height': 500,
            'title': {
                'text': "Статистика",
                'fontSize': 24
            },
            'axisY': {
                'title': "Количество"
            },
            'data': [{
                'type': "column",
                'toolTipContent': "{label}: {y}",
                'dataPoints': points}
            ]
        }}
        json_data = json.dumps(data)
        return HttpResponse(json_data, content_type='application/json')
    else:
        raise Http404
