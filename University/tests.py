from django.test import TestCase
from django.urls import reverse
from University import models


class UniverTest(TestCase):
    def test_professor_list(self):
        url = reverse("professor-list")
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_create_detail(self):
        obj = models.Faculty.objects.create(name="Тестовый факультет")
        url = reverse("faculty-detail", args=(obj.id,))
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_create_change(self):
        """
        Должен фэйлить, поскольку доступ только для авторизованных пользователей.
        """
        obj = models.Faculty.objects.create(name="Тестовый факультет")
        url = reverse("faculty-update", args=(obj.id,))
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
