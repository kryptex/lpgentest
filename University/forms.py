from django import forms
from django.contrib.auth import forms as auth_forms
from University import models


class AuthenticationForm(auth_forms.AuthenticationForm):
    """
    Переопределение виджетов и лейблов для более красивого отображения.
    """
    def __init__(self, *args, **kwargs):
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'placeholder': 'Имя пользователя'})
        self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': 'Пароль'})
        self.fields['username'].label = ''
        self.fields['password'].label = ''


class UserCreationForm(auth_forms.UserCreationForm):
    """
    Переопределение виджетов и лейблов для более красивого отображения.
    """
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'placeholder': 'Имя пользователя'})
        self.fields['password1'].widget = forms.PasswordInput(attrs={'placeholder': 'Пароль'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'placeholder': 'Подтверждение пароля'})
        self.fields['username'].label = ''
        self.fields['password1'].label = ''
        self.fields['password2'].label = ''
        self.fields['username'].help_text = 'Обязательное поле. Не более 150 символов.<br/>Только буквы, цифры и символы @/./+/-/_.'


class ProfessorForm(forms.ModelForm):
    """
    Переопределение виджетов для удобства выбора.
    """
    class Meta:
        model = models.Professor
        fields = '__all__'
        widgets = {
            'faculty': forms.Select(attrs={'size': 5}),
            'subject': forms.Select(attrs={'size': 5})
        }


class FacultyForm(forms.ModelForm):
    class Meta:
        model = models.Faculty
        fields = '__all__'


class SubjectForm(forms.ModelForm):
    class Meta:
        model = models.Subject
        fields = '__all__'


class StudentForm(forms.ModelForm):
    class Meta:
        model = models.Student
        fields = '__all__'
