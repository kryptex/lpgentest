from django.db import models
from django.urls import reverse


class BaseModel(models.Model):
    """
    Базовая модель для того, чтобы у всех наследников был метод get_absolute_url
    """
    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse(self._meta.object_name.lower() + '-detail', kwargs={'pk': self.pk})


class Faculty(BaseModel):
    name = models.CharField(max_length=100, verbose_name="название факультета", unique=True)

    class Meta:
        verbose_name = "Факультет"
        verbose_name_plural = "Факультеты"


class Subject(BaseModel):
    name = models.CharField(max_length=100, verbose_name="название предмета", unique=True)

    class Meta:
        verbose_name = "Предмет"
        verbose_name_plural = "Предметы"


class Professor(BaseModel):
    name = models.CharField(max_length=100, verbose_name="Ф.И.О. профессора", unique=True)
    faculty = models.ForeignKey(Faculty, verbose_name=Faculty._meta.verbose_name)
    subject = models.ForeignKey(Subject, verbose_name=Subject._meta.verbose_name, blank=True, null=True)

    class Meta:
        verbose_name = "Профессор"
        verbose_name_plural = "Профессора"


class Student(BaseModel):
    name = models.CharField(max_length=100, verbose_name="Ф.И.О. студента", unique=True)

    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Студенты"


class SubjectStudent(BaseModel):
    subject = models.ForeignKey(Subject, verbose_name=Subject._meta.verbose_name)
    student = models.ForeignKey(Student, verbose_name=Student._meta.verbose_name)

    class Meta:
        unique_together = ('subject', 'student')
        verbose_name = "Студент - Предмет"
        verbose_name_plural = "Студенты - Предметы"

    def __str__(self):
        return ' - '.join([str(self.subject), str(self.student)])
