from django.contrib import admin
from University import models, forms


@admin.register(models.Faculty)
class FacultyAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering = ['name']


@admin.register(models.Professor)
class ProfessorAdmin(admin.ModelAdmin):
    list_display = ['name', 'faculty', 'subject']
    list_filter = ['faculty', 'subject']
    ordering = ['name']
    form = forms.ProfessorForm
    change_form_template = 'admin.html'  # переопределение шаблона для того, чтобы изменить стили select-полей


class SSInline(admin.StackedInline):
    """
    Инлайн для определения взаимосвязи между таблицами Предмет и Студент.
    """
    model = models.SubjectStudent
    extra = 1


@admin.register(models.Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering = ['name']
    inlines = [SSInline]


@admin.register(models.Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['name']
    ordering = ['name']
    inlines = [SSInline]
