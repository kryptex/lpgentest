$(function () {
    
    $.ajax({
        type: "GET",
        url: "/get_chart/",
        success: function (data) {
            $("#piechart").CanvasJSChart(
                    data.chart
            );
        },
        error: function (error) {
            console.log('error' + error);
        }
    });
    
});