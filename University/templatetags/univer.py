from django import template
from django.urls import reverse_lazy

register = template.Library()


@register.filter(name='suffix')
def suffix(value):
    """
    Корректировка склонения передаваемого title объекта из шаблона.
    """
    suffixes = {
        'факультеты': 'факультет',
        'предметы': 'предмет',
        'профессор': 'профессора',
        'профессоры': 'профессора',
        'студент': 'студента',
        'студенты': 'студента'
    }
    value = value.lower()
    return suffixes.get(value, value)
