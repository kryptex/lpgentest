from django.conf.urls import url
from django.contrib import admin
from University import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', views.Login.as_view(), name='login'),
    url(r'^accounts/logout/$', views.Logout.as_view(), name='logout'),
    url(r'^accounts/register/$', views.Register.as_view(), name='register'),
    url(r'^$', views.Index.as_view(), name="index"),

    url(r'^professor/$', views.ProfessorList.as_view(), name="professor-list"),
    url(r'^faculty/$', views.FacultyList.as_view(), name="faculty-list"),
    url(r'^student/$', views.StudentList.as_view(), name="student-list"),
    url(r'^subject/$', views.SubjectList.as_view(), name="subject-list"),

    url(r'^professor/(?P<pk>\d+)/detail/$', views.ProfessorDetail.as_view(), name="professor-detail"),
    url(r'^faculty/(?P<pk>\d+)/detail/$', views.FacultyDetail.as_view(), name="faculty-detail"),
    url(r'^student/(?P<pk>\d+)/detail/$', views.StudentDetail.as_view(), name="student-detail"),
    url(r'^subject/(?P<pk>\d+)/detail/$', views.SubjectDetail.as_view(), name="subject-detail"),

    url(r'^professor/add/$', views.ProfessorCreate.as_view(), name="professor-create"),
    url(r'^faculty/add/$', views.FacultyCreate.as_view(), name="faculty-create"),
    url(r'^student/add/$', views.StudentCreate.as_view(), name="student-create"),
    url(r'^subject/add/$', views.SubjectCreate.as_view(), name="subject-create"),

    url(r'^professor/(?P<pk>\d+)/change/$', views.ProfessorUpdate.as_view(), name="professor-update"),
    url(r'^faculty/(?P<pk>\d+)/change/$', views.FacultyUpdate.as_view(), name="faculty-update"),
    url(r'^student/(?P<pk>\d+)/change/$', views.StudentUpdate.as_view(), name="student-update"),
    url(r'^subject/(?P<pk>\d+)/change/$', views.SubjectUpdate.as_view(), name="subject-update"),

    url(r'^professor/(?P<pk>\d+)/delete/$', views.ProfessorDelete.as_view(), name="professor-delete"),
    url(r'^faculty/(?P<pk>\d+)/delete/$', views.FacultyDelete.as_view(), name="faculty-delete"),
    url(r'^student/(?P<pk>\d+)/delete/$', views.StudentDelete.as_view(), name="student-delete"),
    url(r'^subject/(?P<pk>\d+)/delete/$', views.SubjectDelete.as_view(), name="subject-delete"),

    url(r'^get_chart/', views.get_chart, name="get_chart"),

]
